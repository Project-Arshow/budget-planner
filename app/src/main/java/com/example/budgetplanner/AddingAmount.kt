package com.example.budgetplanner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class AddingAmount : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adding_amount)

        val amountInput = findViewById<EditText>(R.id.amountInput)
        amountInput.setRawInputType(InputType.TYPE_CLASS_NUMBER)

        val amountAddButton = findViewById<Button>(R.id.amountAddButton)
        amountAddButton.setOnClickListener(View.OnClickListener {
            Toast.makeText(this, "Amount Added !", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        })
    }
}
