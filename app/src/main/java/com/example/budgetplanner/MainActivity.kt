package com.example.budgetplanner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val add = findViewById<Button>(R.id.addButton)
        add.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, AddingAmount::class.java)
            startActivity(intent)
        })

        val minus = findViewById<Button>(R.id.minusButton)
        minus.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, RemoveAmount::class.java)
            startActivity(intent)
        })
    }
}