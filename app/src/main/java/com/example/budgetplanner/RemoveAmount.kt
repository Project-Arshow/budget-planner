package com.example.budgetplanner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class RemoveAmount : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_remove_amount)

        val amountRemoveInput = findViewById<EditText>(R.id.amountRemoveInput)
        amountRemoveInput.setRawInputType(InputType.TYPE_CLASS_NUMBER)

        val amountRemoveButton = findViewById<Button>(R.id.amountRemoveButton)
        amountRemoveButton.setOnClickListener(View.OnClickListener {
            Toast.makeText(this, "Amount Removed !", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        })
    }
}