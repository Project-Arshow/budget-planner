package com.example.budgetplanner

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val amount_query = ("CREATE TABLE " + AMOUNT_TABLE_NAME + " ("
                + AMOUNT_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                AMOUNT_DATE_COL + " TEXT," +
                AMOUNT_BUDGET_COL + " FLOAT" + ")")

        val op_query = ("CREATE TABLE " + OP_TABLE_NAME + " ("
                + OP_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                OP_DATE_COL + " TEXT," +
                OP_AMOUNT_COL + " FLOAT" + ")")

        db.execSQL(amount_query)
        db.execSQL(op_query)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + AMOUNT_TABLE_NAME)
        db.execSQL("DROP TABLE IF EXISTS " + OP_TABLE_NAME)
        onCreate(db)
    }

    fun addAmount(date : Date, amount : Int ){

        val values = ContentValues()
        val currentDate: String = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())


        values.put(OP_DATE_COL, currentDate)
        values.put(OP_AMOUNT_COL, amount)

        val db = this.writableDatabase

        db.insert(OP_TABLE_NAME, null, values)

        db.close()
    }

    fun getAmount(): Cursor? {
        val db = this.readableDatabase

        return db.rawQuery("SELECT * FROM " + OP_TABLE_NAME, null)
    }

    companion object{
        private val DATABASE_NAME = "BUDGET"

        private val DATABASE_VERSION = 1

        //TABLE containing amount update
        val AMOUNT_TABLE_NAME = "AMOUNT"
        val AMOUNT_ID_COL = "amount_id"
        val AMOUNT_DATE_COL = "amount_date"
        val AMOUNT_BUDGET_COL = "amount_budget"

        //TABLE containing added and lost amounts
        val OP_TABLE_NAME = "OPERATIONS"
        val OP_ID_COL = "op_id"
        val OP_DATE_COL = "op_date"
        val OP_AMOUNT_COL = "op_amount"


    }
}


